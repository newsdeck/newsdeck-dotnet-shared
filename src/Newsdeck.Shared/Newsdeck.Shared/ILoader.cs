using System;
using Microsoft.Extensions.DependencyInjection;

namespace Newsdeck.Shared
{
    public interface ILoader
    {
        void Register(IServiceCollection serviceCollection);
        Type GetExecutorType();
    }
}