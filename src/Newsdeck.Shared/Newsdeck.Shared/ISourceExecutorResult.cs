using System.Collections.Generic;
using Newsdeck.Data.Model;

namespace Newsdeck.Shared
{
    public interface ISourceExecutorResult
    {
        IEnumerable<ICreatePublicationModel> ToCreate { get; }
        IDictionary<long, IEditPublicationModel> ToEdit { get; }
        IEnumerable<long> ToDelete { get; }
    }
}