using System;
using System.Collections.Generic;
using System.Linq;

namespace Newsdeck.Shared
{
    /// <summary>
    /// DateTimeOffset extensions
    /// </summary>
    public static class DateTimeOffsetExtension
    {
        /// <summary>
        /// Get a list of time between given dates
        /// If the list contains 3 DateTimeOffset, the returned list will contains 2 TimeSpan
        /// </summary>
        /// <param name="dates"></param>
        /// <returns></returns>
        public static IEnumerable<TimeSpan> GetTimeBetweenDates(this IEnumerable<DateTimeOffset> dates)
        {
            var queue = new Stack<DateTimeOffset>();
            var timesBetween = new List<double>();
            foreach (var date in dates)
            {
                TimeSpan timeBetween;
                if (queue.TryPeek(out var previousDate))
                {
                    timeBetween = date - previousDate;
                }
                else
                {
                    timeBetween = DateTimeOffset.Now - date;
                }
                var secondsBetween = Math.Abs(timeBetween.TotalSeconds);
                timesBetween.Add(secondsBetween);
                queue.Push(date);
            }
            queue.Clear();
            return timesBetween.Select(TimeSpan.FromSeconds);
        }
    }
}