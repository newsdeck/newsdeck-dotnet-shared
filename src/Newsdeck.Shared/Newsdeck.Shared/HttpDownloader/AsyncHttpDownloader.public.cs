using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Newsdeck.Shared.HttpDownloader
{
    public partial class AsyncHttpDownloader : IDisposable
    {
        
        public int CountRetryOnError { get; }

        public AsyncHttpDownloader(TimeSpan? timoutTimeSpan = null, int? countRetryOnError = 4)
        {
            _httpClient = new HttpClient(new HttpClientHandler
            {
                AllowAutoRedirect = false
            })
            {
                Timeout = timoutTimeSpan ?? TimeSpan.FromSeconds(5)
            };
            _httpClient.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0");
            CountRetryOnError = countRetryOnError ?? 4;
        }

        public void AddDefaultRequestHeader(string name, string value)
        {
            _httpClient.DefaultRequestHeaders.Add(name, value);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="OperationCanceledException"></exception>
        public Task<IHttpDownloadResult> GetAsync(string url, CancellationToken cancellationToken)
        {
            return _GetStreamAsync(url, cancellationToken);
        }
    }
}