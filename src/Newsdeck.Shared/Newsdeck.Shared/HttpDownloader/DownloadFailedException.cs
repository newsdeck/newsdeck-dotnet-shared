using System;

namespace Newsdeck.Shared.HttpDownloader
{
    public class DownloadFailedException : Exception
    {
        public DownloadFailedException(Uri url, Exception innerExpression)
        : base($"Download at url '{url}' could not finish", innerExpression)
        {
            
        }
    }
}