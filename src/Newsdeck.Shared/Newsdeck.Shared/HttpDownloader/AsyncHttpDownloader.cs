using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Newsdeck.Shared.HttpDownloader
{
    public partial class AsyncHttpDownloader
    {
        private class ExecutionCount
        {
            public int ErrorRetry { get; set; }
            public int CountRedirect { get; set; }
        }
        
        private readonly HttpClient _httpClient;
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="OperationCanceledException"></exception>
        private Task<IHttpDownloadResult> _GetStreamAsync(string url, CancellationToken cancellationToken)
        {
            return _GetStreamWithRetryAsync(new Uri(url), new ExecutionCount(), cancellationToken);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <param name="tries"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="OperationCanceledException"></exception>
        private async Task<IHttpDownloadResult> _GetStreamWithRetryAsync(Uri url, ExecutionCount tries, CancellationToken cancellationToken)
        {
            try
            {
                var response = await LoadResponse(url, cancellationToken);
                if (response.IsSuccessStatusCode)
                {
                    return new HttpDownloadResult(response.Content)
                    {
                        ContentType = response.Content?.Headers?.ContentType?.MediaType,
                        EncodingType = response.Content?.Headers?.ContentEncoding?.FirstOrDefault()
                    };
                }

                switch (response.StatusCode)
                {
                    case HttpStatusCode.Moved: // 301
                    case HttpStatusCode.Redirect: // 302
                    case HttpStatusCode.RedirectMethod: // 303
                    case HttpStatusCode.TemporaryRedirect: // 307
                    case HttpStatusCode.PermanentRedirect: // 308
                        var urlRedirect = response.Headers.Location;
                        if (!urlRedirect.IsAbsoluteUri)
                        {
                            urlRedirect = new Uri(url, urlRedirect);
                        }
                        if (tries.CountRedirect > 10)
                        {
                            return new HttpDownloadResult(null)
                            {
                                Error = new ApplicationException("Max redirection count has been reached")
                            };
                        }
                        tries.CountRedirect++;
                        return await _GetStreamWithRetryAsync(urlRedirect, tries, cancellationToken); // from 0 as is is redirect
                    default:
                        return new HttpDownloadResult(null)
                        {
                            Error = new ApplicationException(
                                $"StatusCode: {response.StatusCode}, message: {response.ReasonPhrase}")
                        };
                }
            }
            catch (OperationCanceledException e)
            {
                throw; // we keep it here just to remind ourself it exists
            }
            catch (DownloadFailedException e)
            {
                if (tries.ErrorRetry >= CountRetryOnError)
                {
                    return new HttpDownloadResult(null)
                    {
                        Error = e
                    };
                }
                else
                {
                    tries.ErrorRetry++;
                    return await _GetStreamWithRetryAsync(url, tries, cancellationToken);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="TaskCanceledException"></exception>
        /// <exception cref="OperationCanceledException"></exception>
        private async Task<HttpResponseMessage> LoadResponse(Uri url, CancellationToken cancellationToken)
        {
            try
            {
                var res = await _httpClient.GetAsync(url, cancellationToken);
                cancellationToken.ThrowIfCancellationRequested();
                return res;
            }
            catch (TaskCanceledException e)
            {
                throw new DownloadFailedException(url, e);
            }
            catch (OperationCanceledException e)
            {
                // Why are they throwing OperationCanceledException if its not coming from CancellationToken?
                if (cancellationToken.IsCancellationRequested)
                {
                    throw;
                }
                throw new DownloadFailedException(url, e);
            }
            catch (HttpRequestException e)
            {
                throw new DownloadFailedException(url, e);
            }
        }

        public void Dispose()
        {
            _httpClient?.CancelPendingRequests();
            _httpClient?.Dispose();
        }
    }
}