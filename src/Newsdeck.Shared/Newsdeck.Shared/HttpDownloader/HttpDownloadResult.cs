using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Newsdeck.Shared.NewsdeckBusinessException;

namespace Newsdeck.Shared.HttpDownloader
{
    public interface IHttpDownloadResult
    { 
        /// <summary>
        /// Was there any issue
        /// </summary>
        bool IsInError { get; }
        System.Exception Error { get; }
        string ContentType { get; }
        string EncodingType { get; }

        Task<Stream> GetResultStreamAsync();
        Task<string> GetResultContentAsync();
        Task GetResultInFileAsync(string destinationPath);
    }
    
    internal class HttpDownloadResult : IHttpDownloadResult
    {

        private readonly HttpContent _content;
        
        internal HttpDownloadResult(HttpContent content)
        {
            _content = content;
        }
        
        public bool IsInError => Error != null && _content == null;
        public System.Exception Error { get; set; }
        public string ContentType { get; set; }
        public string EncodingType { get; set; }
        
        public Task<Stream> GetResultStreamAsync()
        {
            return _content?.ReadAsStreamAsync();
        }

        public Task<string> GetResultContentAsync()
        {
            return _content?.ReadAsStringAsync();
        }

        public async Task GetResultInFileAsync(string destinationPath)
        {
            if (File.Exists(destinationPath))
            {
                throw new FileAlreadyExistsException(destinationPath);
            }
            var rootDir = Path.GetDirectoryName(destinationPath);
            if (string.IsNullOrWhiteSpace(rootDir))
            {
                throw new InvalidFilePathException("Directory name is invalid");
            }
            Directory.CreateDirectory(rootDir); // ensure directory is created
            await using var streamDestination = File.Create(destinationPath); // Create file path
            await _content.CopyToAsync(streamDestination);
        }
    }
}