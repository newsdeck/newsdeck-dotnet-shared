using System.Threading;
using System.Threading.Tasks;
using Newsdeck.Data.Entity;

namespace Newsdeck.Shared
{
    public interface ISourceExecutor
    {
        Task<ISourceExecutorResult> LoadArticlesAsync(ISourceEntity entity, CancellationToken cancellationToken);
        Task<int> CheckHandlingCapabilitiesForInputAsync(string inputUrl, CancellationToken cancellationToken);
    }
}