using System;

namespace Newsdeck.Shared.NewsdeckBusinessException
{
    [Serializable]
    public class InvalidFilePathException : System.Exception
    {
        public InvalidFilePathException(string message)
            : base(message)
        {
            
        }
    }
}