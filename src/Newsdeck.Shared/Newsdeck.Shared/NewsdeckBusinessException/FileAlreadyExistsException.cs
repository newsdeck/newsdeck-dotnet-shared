using System;

namespace Newsdeck.Shared.NewsdeckBusinessException
{
    [Serializable]
    public class FileAlreadyExistsException : System.Exception
    {
        public FileAlreadyExistsException(string path)
        : base($"File at path '{path}' already exists, remove the existing file before proceeding")
        {
            
        }
    }
}