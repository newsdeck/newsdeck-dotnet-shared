using System;

namespace Newsdeck.Shared.NewsdeckBusinessException
{
    [Serializable]
    public class MissingSourceExecutorException : System.Exception
    {
        public MissingSourceExecutorException(string sourceType)
            : base ($"Executor for type '{sourceType}' can not be found, either sourceType is wrong or executor is not registered")
        {
            
        }
    }
}